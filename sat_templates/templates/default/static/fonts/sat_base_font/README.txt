This font is based on the Alegreya font by Juan Pablo del Peral (juan@huertatipografica.com.ar), it has been modified to be compressed using https://google-webfonts-helper.herokuapp.com, and put here for being embedded in Salut à Toi templates, to avoid privacy issues with third party website download.

As the name is a reserved name and the compression make the font a derivative, this font has been renamed to "sat_base_font", according to the recommandation read at http://scripts.sil.org/cms/scripts/page.php?item_id=OFL-FAQ_web#1db14da7

Thanks to Juan Pable del Peral for his work!

The license "SIL Open Font License, Version 1.1" only apply to this directory.
